"use strict";
exports.__esModule = true;
var effects_1 = require("@effects/effects");
var ConsoleLogBuilder = /** @class */ (function () {
    function ConsoleLogBuilder() {
    }
    ConsoleLogBuilder.prototype.addLog = function (log) {
        console.log(log);
    };
    return ConsoleLogBuilder;
}());
exports.ConsoleLogBuilder = ConsoleLogBuilder;
var JsonLogBuilder = /** @class */ (function () {
    function JsonLogBuilder() {
    }
    JsonLogBuilder.prototype.addLog = function (log) {
        return;
    };
    return JsonLogBuilder;
}());
exports.JsonLogBuilder = JsonLogBuilder;
var Engine = /** @class */ (function () {
    function Engine(logger) {
        var _this = this;
        this.logger = logger;
        this.useSkill = function (actor, skill, target) {
            var damage = _this.applyCombatBuffs(actor, skill);
            target.hp = target.hp - damage;
            if (damage > 0) {
                target.hp = target.hp - damage;
                _this.logger.addLog("Action: " + actor.name + " dealt " + damage + " to " + target.name + " with " + skill.name);
            }
            _this.applyEffects(actor, target, skill.effects);
            if (skill.cooldown) {
                actor.addCoolDown(skill);
            }
        };
        this.applyDebuffs = function (phase) { return function (actor) {
            var toApply = actor.debuffs.filter(function (el) { return el.effect.phase === phase; });
            for (var i = 0; i < toApply.length; i++) {
                if (toApply[i].skip) {
                    toApply[i].skip = false;
                }
                else {
                    toApply[i].effect.apply(actor);
                    toApply[i].turns--;
                }
                if (toApply[i].turns <= 0) {
                    _this.logger.addLog("Effect: " + actor.debuffs[i].effect.name + " fades from " + actor.name);
                }
            }
        }; };
        this.applyBuffs = function (phase) { return function (actor, skill) {
            var buffsToApply = actor.buffs.filter(function (el) { return el.effect.phase === phase; });
            var damage = skill.damage;
            for (var i = 0; i < buffsToApply.length; i++) {
                damage = buffsToApply[i].effect.apply(skill, damage, actor);
                actor.buffs[i].turns--;
                if (actor.buffs[i].turns <= 0) {
                    _this.logger.addLog("Effect: " + buffsToApply[i].effect.name + " fades from " + actor.name);
                }
            }
            actor.buffs = actor.buffs.filter(function (el) { return el.turns > 0; });
            return damage;
        }; };
        this.applyEffects = function (actor, target, effects) {
            effects.forEach(function (effect) {
                switch (effect.type) {
                    case effects_1.EffectType.Buff:
                        _this.addBuff(actor, effect);
                        break;
                    case effects_1.EffectType.Debuff:
                        _this.addDebuff(actor, target, effect);
                        break;
                    default:
                        console.error('ERR: UNKNOWN EFFECT');
                }
            });
        };
        this.addDebuff = function (actor, target, effect) {
            var reDebuff = actor.debuffs.find(function (el) { return el.effect.name === effect.name; });
            if (reDebuff) {
                reDebuff.turns = effect.turns;
                _this.logger.addLog(actor.name + " refreshes " + effect.name + " to " + target.name);
            }
            else {
                target.debuffs.push({
                    effect: effect,
                    skip: effect.phase === effects_1.Phase.PostCombat,
                    turns: effect.turns
                });
                _this.logger.addLog(actor.name + " applies " + effect.name + " to " + target.name);
            }
        };
        this.addBuff = function (actor, effect) {
            var reBuff = actor.buffs.find(function (el) { return el.effect.name === effect.name; });
            if (reBuff) {
                reBuff.turns = effect.turns;
                _this.logger.addLog("Effect: " + actor.name + " refreshes " + effect.name + " on self");
            }
            else {
                actor.buffs.push({ effect: effect, skip: false, turns: effect.turns });
                _this.logger.addLog("Effect: " + actor.name + " gains " + effect.name);
            }
        };
        this.applyPreCombatDebuffs = this.applyDebuffs(effects_1.Phase.PreCombat);
        this.applyCombatDebuffs = this.applyDebuffs(effects_1.Phase.PostCombat);
        this.applyPostCombatDebuffs = this.applyDebuffs(effects_1.Phase.PostCombat);
        this.applyPreCombatBuffs = this.applyBuffs(effects_1.Phase.PreCombat);
        this.applyCombatBuffs = this.applyBuffs(effects_1.Phase.Combat);
        this.applyPostCombatBuffs = this.applyBuffs(effects_1.Phase.PostCombat);
    }
    return Engine;
}());
exports.Engine = Engine;
