import {Actor} from '@models/actor/actor';
import {Buff, Debuff, Effect, EffectType, Phase} from '@effects/effects';
import {Skill} from '@skills/skill';

export interface CombatLogBuilder {
    addLog: (log: string) => void;

}

export class ConsoleLogBuilder implements CombatLogBuilder {
    addLog(log) {
        console.log(log);
    }
}

export class JsonLogBuilder implements CombatLogBuilder {
    addLog(log) {
        return;
    }
}

export class Engine {

    constructor(private logger: CombatLogBuilder) {
    }

    useSkill = (actor: Actor, skill: Skill, target: Actor) => {
        const damage = this.applyCombatBuffs(actor, skill);

        target.hp = target.hp - damage;

        if (damage > 0) {
            target.hp = target.hp - damage;
            this.logger.addLog(`Action: ${actor.name} dealt ${damage} to ${target.name} with ${skill.name}`);
        }

        this.applyEffects(actor, target, skill.effects);

        if (skill.cooldown) {
            actor.addCoolDown(skill);
        }
    };

    applyDebuffs = (phase: Phase) => (actor: Actor) => {
        const toApply = actor.debuffs.filter(el => el.effect.phase === phase);

        for (let i = 0; i < toApply.length; i++) {
            if (toApply[i].skip) {
                toApply[i].skip = false;
            } else {
                toApply[i].effect.apply(actor);
                toApply[i].turns--;
            }

            if (toApply[i].turns <= 0) {
                this.logger.addLog(`Effect: ${actor.debuffs[i].effect.name} fades from ${actor.name}`);
            }
        }
    };

    applyBuffs = (phase: Phase) => (actor: Actor, skill: Skill) => {
        const buffsToApply = actor.buffs.filter(el => el.effect.phase === phase);
        let damage = skill.damage;

        for (let i = 0; i < buffsToApply.length; i++) {
            damage = buffsToApply[i].effect.apply(skill, damage, actor);
            actor.buffs[i].turns--;

            if (actor.buffs[i].turns <= 0) {
                this.logger.addLog(`Effect: ${buffsToApply[i].effect.name} fades from ${actor.name}`);
            }
        }

        actor.buffs = actor.buffs.filter(el => el.turns > 0);

        return damage;
    };

    applyEffects = (actor: Actor, target: Actor, effects: Effect[]) => {
        effects.forEach(effect => {
            switch (effect.type) {
                case EffectType.Buff:
                    this.addBuff(actor, <Buff>effect);
                    break;
                case EffectType.Debuff:
                    this.addDebuff(actor, target, <Debuff>effect);
                    break;
                default:
                    console.error('ERR: UNKNOWN EFFECT');
            }
        });
    };

    addDebuff = (actor: Actor, target: Actor, effect: Debuff) => {
        const reDebuff = actor.debuffs.find(el => el.effect.name === effect.name);

        if (reDebuff) {
            reDebuff.turns = effect.turns;
            this.logger.addLog(`${actor.name} refreshes ${effect.name} to ${target.name}`);
        } else {
            target.debuffs.push({
                effect: effect,
                skip: effect.phase === Phase.PostCombat,
                turns: effect.turns
            });
            this.logger.addLog(`${actor.name} applies ${effect.name} to ${target.name}`);
        }
    };

    addBuff = (actor: Actor, effect: Buff) => {
        const reBuff = actor.buffs.find(el => el.effect.name === effect.name);

        if (reBuff) {
            reBuff.turns = effect.turns;
            this.logger.addLog(`Effect: ${actor.name} refreshes ${effect.name} on self`);
        } else {
            actor.buffs.push({effect: effect, skip: false, turns: effect.turns});
            this.logger.addLog(`Effect: ${actor.name} gains ${effect.name}`);
        }
    };

    applyPreCombatDebuffs = this.applyDebuffs(Phase.PreCombat);
    applyCombatDebuffs = this.applyDebuffs(Phase.PostCombat);
    applyPostCombatDebuffs = this.applyDebuffs(Phase.PostCombat);

    applyPreCombatBuffs = this.applyBuffs(Phase.PreCombat);
    applyCombatBuffs = this.applyBuffs(Phase.Combat);
    applyPostCombatBuffs = this.applyBuffs(Phase.PostCombat);
}