"use strict";
exports.__esModule = true;
var EffectType;
(function (EffectType) {
    EffectType[EffectType["Debuff"] = 0] = "Debuff";
    EffectType[EffectType["Buff"] = 1] = "Buff";
})(EffectType = exports.EffectType || (exports.EffectType = {}));
var Phase;
(function (Phase) {
    Phase[Phase["PreCombat"] = 0] = "PreCombat";
    Phase[Phase["Combat"] = 1] = "Combat";
    Phase[Phase["PostCombat"] = 2] = "PostCombat";
})(Phase = exports.Phase || (exports.Phase = {}));
var Status;
(function (Status) {
    Status["Confusion"] = "confused";
    Status["Stun"] = "stunned";
})(Status = exports.Status || (exports.Status = {}));
exports.damageOverTime = function (damage, name) { return function (character) {
    character.hp = character.hp - damage;
    console.log("Effect: " + character.name + " " + (damage > 0 ? 'takes' : 'heals') + " " + (damage > 0 ? damage : damage * -1) + " from " + name);
}; };
exports.damageModifier = function (damage, name) { return function (currentDamage, character) {
    console.log("Effect: " + character.name + " " + (damage > 0 ? 'gains' : 'loses') + " " + (damage > 0 ? damage : damage * -1) + " damage from " + name);
    return currentDamage + damage;
}; };
exports.damageMultiplier = function (multiplier, name) { return function (currentDamage, character) {
    console.log("Effect: " + character.name + " gains " + multiplier + "x damage from " + name);
    return currentDamage + multiplier;
}; };
exports.applyStatus = function (status, name) { return function (character) {
    if (character.status === Status.Stun) {
        console.log("Effect: " + character.name + " is still " + status + " by " + name);
    }
    else {
        console.log("Effect: " + character.name + " is " + status + " by " + name);
    }
    character.status = Status.Stun;
}; };
