import {Actor} from '@models/actor/actor';

export enum EffectType {
    Debuff,
    Buff,
}

export enum Phase {
    PreCombat,
    Combat,
    PostCombat
}

export enum Status {
    Confusion = 'confused',
    Stun = 'stunned'
}

export interface Effect {
    name: string;
    apply: ((...args) => any);
    turns: number;
    phase: Phase;
    type: EffectType;
}

export interface Buff extends Effect {
    type: EffectType.Buff;
}

export interface Debuff extends Effect {
    type: EffectType.Debuff;
}

export const damageOverTime = (damage: number, name: string) => (character: Actor) => {
    character.hp = character.hp - damage;
    console.log(`Effect: ${character.name} ${damage > 0 ? 'takes' : 'heals'} ${damage > 0 ? damage : damage * -1} from ${name}`);
};

export const damageModifier = (damage: number, name: string) => (currentDamage: number, character: Actor) => {
    console.log(`Effect: ${character.name} ${damage > 0 ? 'gains' : 'loses'} ${damage > 0 ? damage : damage * -1} damage from ${name}`);
    return currentDamage + damage;
};

export const damageMultiplier = (multiplier: number, name: string) => (currentDamage: number, character: Actor) => {
    console.log(`Effect: ${character.name} gains ${multiplier}x damage from ${name}`);
    return currentDamage + multiplier;
};

export const applyStatus = (status: Status, name: string) => (character: Actor) => {
    if(character.status === Status.Stun) {
        console.log(`Effect: ${character.name} is still ${status} by ${name}`);
    } else {
        console.log(`Effect: ${character.name} is ${status} by ${name}`);
    }

    character.status = Status.Stun;
}
