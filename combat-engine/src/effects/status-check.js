"use strict";
exports.__esModule = true;
var effects_1 = require("@effects/effects");
function canAct(actor) {
    switch (actor.status) {
        case effects_1.Status.Stun:
            return false;
        default:
            return true;
    }
}
exports.canAct = canAct;
