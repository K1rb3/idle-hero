import {Actor} from '@models/actor/actor';
import {Status} from '@effects/effects';


export function canAct(actor: Actor) {
    switch (actor.status) {
        case Status.Stun:
            return false;
        default:
            return true;
    }
}