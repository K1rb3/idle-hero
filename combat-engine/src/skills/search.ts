import {Skill} from '@skills/skill';

export const searchSkills = (skillList: Skill[]) => (...ids: string[]): Skill[] => {
    if(ids.length === 1) {
        return [skillList.find(el => el.id === ids[0])];
    }

    const skills = [];

    for(let i = 0;i< ids.length;i++){
        skills.push(skillList.find(el => el.id === ids[i]));
    }

    return skills;
};