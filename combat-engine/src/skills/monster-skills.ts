import {Skill} from './skill';
import {Modifier} from '@models/Skill/modifier';
import {damageOverTime, EffectType, Phase, damageMultiplier} from '@effects/effects';
import {searchSkills} from '@skills/search';

const skills: Skill[] = [
    {
        id: 'MON001',
        name: 'Bite',
        cooldown: 0,
        damage: 1,
        modifier: Modifier.strength,
        modifierRate: 0.5,
        effects: [],
    },
    {
        id: 'MON002',
        name: 'Claw',
        cooldown: 5,
        damage: 2,
        effects: [{
            apply: damageOverTime(2, 'Decay'),
            name: 'Decay',
            turns: 4,
            type: EffectType.Debuff,
            phase: Phase.PostCombat
        }]
    },
    {
        id: 'MON003',
        name: 'Vicious Assault',
        cooldown: 5,
        damage: 2,
        effects: [{
            apply: damageMultiplier(2, 'Ferocity'),
            name: 'Ferocity',
            turns: 1,
            type: EffectType.Buff,
            phase: Phase.PostCombat
        }]
    }
];

skills.forEach(el => Object.freeze(el));

export const getMonsterSkills = searchSkills(skills);
