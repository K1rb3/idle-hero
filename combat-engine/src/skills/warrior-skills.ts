import {Skill} from './skill';
import {Modifier} from '@models/Skill/modifier';
import {
    damageOverTime,
    damageModifier,
    EffectType,
    Phase,
    applyStatus,
    Status
} from '@effects/effects';
import {searchSkills} from '@skills/search';

const skills: Skill[] = [
    {
        id: 'WAR001',
        name: 'Strike',
        cooldown: 0,
        damage: 1,
        modifier: Modifier.strength,
        modifierRate: 1.5,
        effects: [],
    },
    {
        id: 'WAR002',
        name: 'Rend',
        cooldown: 4,
        damage: 1,
        effects: [{
            apply: damageOverTime(1, 'Bleed'),
            turns: 2,
            name: 'Bleed',
            type: EffectType.Debuff,
            phase: Phase.PostCombat
        }]
    },
    {
        id: 'WAR003',
        name: 'Berserk',
        cooldown: 5,
        damage: 0,
        effects: [{
            apply: damageModifier(2, 'Berserk'),
            turns: 2,
            name: 'Berserk',
            type: EffectType.Buff,
            phase: Phase.PostCombat
        }],
    },
    {
        id: 'WAR004',
        name: 'Draining Strike',
        cooldown: 3,
        damage: 10,
        modifier: Modifier.strength,
        modifierRate: 1.5,
        effects: [{
            apply: damageModifier(-1, 'Weaken'),
            turns: 3,
            name: 'Weaken',
            type: EffectType.Buff,
            phase: Phase.PostCombat
        }],
    },
    {
        id: 'WAR005',
        name: 'Bash',
        cooldown: 3,
        damage: 3,
        modifier: Modifier.strength,
        modifierRate: 1.5,
        effects: [{
            apply: applyStatus(Status.Stun, 'Bash'),
            turns: 2,
            name: 'Stun',
            type: EffectType.Debuff,
            phase: Phase.PreCombat
        }],
    },
];

skills.forEach(el => Object.freeze(el));

export const getWarriorSkills = searchSkills(skills);
