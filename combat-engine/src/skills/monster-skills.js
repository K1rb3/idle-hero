"use strict";
exports.__esModule = true;
var modifier_1 = require("@models/Skill/modifier");
var effects_1 = require("@effects/effects");
var search_1 = require("@skills/search");
var skills = [
    {
        id: 'MON001',
        name: 'Bite',
        cooldown: 0,
        damage: 1,
        modifier: modifier_1.Modifier.strength,
        modifierRate: 0.5,
        effects: []
    },
    {
        id: 'MON002',
        name: 'Claw',
        cooldown: 5,
        damage: 2,
        effects: [{
                apply: effects_1.damageOverTime(2, 'Decay'),
                name: 'Decay',
                turns: 4,
                type: effects_1.EffectType.Debuff,
                phase: effects_1.Phase.PostCombat
            }]
    },
    {
        id: 'MON003',
        name: 'Vicious Assault',
        cooldown: 5,
        damage: 2,
        effects: [{
                apply: effects_1.damageMultiplier(2, 'Ferocity'),
                name: 'Ferocity',
                turns: 1,
                type: effects_1.EffectType.Buff,
                phase: effects_1.Phase.PostCombat
            }]
    }
];
skills.forEach(function (el) { return Object.freeze(el); });
exports.getMonsterSkills = search_1.searchSkills(skills);
