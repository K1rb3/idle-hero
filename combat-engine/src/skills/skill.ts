import {Effect} from '@effects/effects';
import {Modifier} from '@models/Skill/modifier';


export interface Skill {
    id: string;
    name: string;
    damage: number;
    effects: Effect[];
    cooldown: number;
    modifier?: Modifier,
    modifierRate?: number;
    healing?: boolean;
}