"use strict";
exports.__esModule = true;
var modifier_1 = require("@models/Skill/modifier");
var effects_1 = require("@effects/effects");
var search_1 = require("@skills/search");
var skills = [
    {
        id: 'WAR001',
        name: 'Strike',
        cooldown: 0,
        damage: 1,
        modifier: modifier_1.Modifier.strength,
        modifierRate: 1.5,
        effects: []
    },
    {
        id: 'WAR002',
        name: 'Rend',
        cooldown: 4,
        damage: 1,
        effects: [{
                apply: effects_1.damageOverTime(1, 'Bleed'),
                turns: 2,
                name: 'Bleed',
                type: effects_1.EffectType.Debuff,
                phase: effects_1.Phase.PostCombat
            }]
    },
    {
        id: 'WAR003',
        name: 'Berserk',
        cooldown: 5,
        damage: 0,
        effects: [{
                apply: effects_1.damageModifier(2, 'Berserk'),
                turns: 2,
                name: 'Berserk',
                type: effects_1.EffectType.Buff,
                phase: effects_1.Phase.PostCombat
            }]
    },
    {
        id: 'WAR004',
        name: 'Draining Strike',
        cooldown: 3,
        damage: 10,
        modifier: modifier_1.Modifier.strength,
        modifierRate: 1.5,
        effects: [{
                apply: effects_1.damageModifier(-1, 'Weaken'),
                turns: 3,
                name: 'Weaken',
                type: effects_1.EffectType.Buff,
                phase: effects_1.Phase.PostCombat
            }]
    },
    {
        id: 'WAR005',
        name: 'Bash',
        cooldown: 3,
        damage: 3,
        modifier: modifier_1.Modifier.strength,
        modifierRate: 1.5,
        effects: [{
                apply: effects_1.applyStatus(effects_1.Status.Stun, 'Bash'),
                turns: 2,
                name: 'Stun',
                type: effects_1.EffectType.Debuff,
                phase: effects_1.Phase.PreCombat
            }]
    },
];
skills.forEach(function (el) { return Object.freeze(el); });
exports.getWarriorSkills = search_1.searchSkills(skills);
