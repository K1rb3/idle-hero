"use strict";
exports.__esModule = true;
var warrior_skills_1 = require("./skills/warrior-skills");
var actor_1 = require("./models/actor/actor");
var monster_skills_1 = require("./skills/monster-skills");
var engine_1 = require("./engine/engine");
var status_check_1 = require("./effects/status-check");
var i = 0;
var LOOP_TIME = 1000;
var finish = false;
var turn = 1;
var heroes = [
    new actor_1.Actor('Steven', 50, 0, warrior_skills_1.getWarriorSkills('WAR005', 'WAR002', 'WAR001'), actor_1.Job.Warrior, 1),
    new actor_1.Actor('John', 50, 0, warrior_skills_1.getWarriorSkills('WAR004', 'WAR003', 'WAR002', 'WAR001'), actor_1.Job.Warrior, 1)
];
var villians = [
    new actor_1.Actor('Wolf 1', 40, 0, monster_skills_1.getMonsterSkills('MON003', 'MON002', 'MON001'), actor_1.Job.Warrior, 2),
    new actor_1.Actor('Wolf 2', 50, 0, monster_skills_1.getMonsterSkills('MON003', 'MON002', 'MON001'), actor_1.Job.Warrior, 2)
];
var actors = heroes.concat(villians);
var heroesFighting = actors.filter(function (el) { return el.team === 1; });
var villansFighting = actors.filter(function (el) { return el.team === 2; });
var getTargets = function (targets, take) {
    return targets.filter(function (el) { return el.alive; }).sort(function (a, b) { return b.threat - a.threat; }).slice(0, take);
};
var logger = new engine_1.ConsoleLogBuilder();
var engine = new engine_1.Engine(logger);
function gameLoop() {
    console.log("------------------Turn " + turn + "--------------------");
    //INITIATIVE
    var turnOrder = actors.filter(function (el) { return el.alive; });
    //COMBAT
    for (var i_1 = 0; i_1 < turnOrder.length; i_1++) {
        var actor = turnOrder[i_1];
        var targets = actor.team === 1 ? getTargets(villansFighting, 1) : getTargets(heroesFighting, 1);
        //PRE-COMBAT
        engine.applyPreCombatDebuffs(actor);
        //COMBAT
        if (status_check_1.canAct(actor)) {
            var skill = actor.getSkill();
            for (var j = 0; j < targets.length; j++) {
                engine.useSkill(actor, skill, targets[j]);
                if (targets[j].hp < 0) {
                    targets[j].alive = false;
                }
            }
        }
        //POST-COMBAT
        engine.applyPostCombatDebuffs(actor);
        actor.progressCooldowns();
    }
    actors.map(function (el) { return el.name + ": " + el.hp + "hp"; }).forEach(function (el) { return console.log(el); });
    //CHECK END
    if (heroesFighting.every(function (x) { return !x.alive; }) || villansFighting.every(function (x) { return !x.alive; })) {
        clearInterval(this);
        finish = true;
        console.log('battle over');
    }
    turn++;
}
//setInterval(gameLoop, LOOP_TIME);
var hrstart = process.hrtime();
while (!finish) {
    gameLoop();
}
var hrEnd = process.hrtime(hrstart);
console.info("Execution time (hr): " + hrEnd[0] + " " + hrEnd[1] / 1000000 + "ms");
