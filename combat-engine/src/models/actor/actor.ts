import {Skill} from '@skills/skill';
import {Buff, Debuff, Status} from '@effects/effects';

export enum Job {
    Warrior
}

export class Actor {
    name: string;
    job: string;
    team: number;
    hp: number;
    energy: number;
    skills: Skill[];
    speed: number;
    intellegence: number;
    strength: number;
    threat: number;
    status: Status;

    cooldowns: { skill: Skill, turnsLeft: number }[] = [];
    buffs: { effect: Buff, skip: boolean, turns: number }[] = [];
    debuffs: { effect: Debuff, skip: boolean, turns: number }[] = [];
    alive = true;

    constructor(name: string, hp: number, energy: number, skills: Skill[], job: Job, team: number) {
        this.name = name;
        this.hp = hp;
        this.energy = energy;
        this.skills = skills;
        this.team = team;
        this.threat = 0;
    }

    addCoolDown(skill: Skill) {
        this.cooldowns.push({skill, turnsLeft: skill.cooldown});
    }

    progressCooldowns() {
        this.cooldowns = this.cooldowns.filter(el => el.turnsLeft > 1);
        for (let i = 0; i < this.cooldowns.length; i++) {
            this.cooldowns[i].turnsLeft--;
        }
    }

    getSkill() {
        return this.skills.find(el => !this.cooldowns.some(el2 => el2.skill === el));
    }
}