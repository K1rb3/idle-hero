"use strict";
exports.__esModule = true;
var Job;
(function (Job) {
    Job[Job["Warrior"] = 0] = "Warrior";
})(Job = exports.Job || (exports.Job = {}));
var Actor = /** @class */ (function () {
    function Actor(name, hp, energy, skills, job, team) {
        this.cooldowns = [];
        this.buffs = [];
        this.debuffs = [];
        this.alive = true;
        this.name = name;
        this.hp = hp;
        this.energy = energy;
        this.skills = skills;
        this.team = team;
        this.threat = 0;
    }
    Actor.prototype.addCoolDown = function (skill) {
        this.cooldowns.push({ skill: skill, turnsLeft: skill.cooldown });
    };
    Actor.prototype.progressCooldowns = function () {
        this.cooldowns = this.cooldowns.filter(function (el) { return el.turnsLeft > 1; });
        for (var i = 0; i < this.cooldowns.length; i++) {
            this.cooldowns[i].turnsLeft--;
        }
    };
    Actor.prototype.getSkill = function () {
        var _this = this;
        return this.skills.find(function (el) { return !_this.cooldowns.some(function (el2) { return el2.skill === el; }); });
    };
    return Actor;
}());
exports.Actor = Actor;
