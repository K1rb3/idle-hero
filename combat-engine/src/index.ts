import {getWarriorSkills} from './skills/warrior-skills';
import {Actor, Job} from './models/actor/actor';
import {getMonsterSkills} from './skills/monster-skills';
import {ConsoleLogBuilder, Engine} from './engine/engine';
import {canAct} from './effects/status-check';


let i = 0;
const LOOP_TIME = 1000;
let finish = false;
let turn = 1;

const heroes = [
    new Actor('Steven', 50, 0, getWarriorSkills('WAR005', 'WAR002', 'WAR001'), Job.Warrior, 1),
    new Actor('John', 50, 0, getWarriorSkills('WAR004', 'WAR003', 'WAR002', 'WAR001'), Job.Warrior, 1)
];

const villians = [
    new Actor('Wolf 1', 40, 0, getMonsterSkills('MON003', 'MON002', 'MON001'), Job.Warrior, 2),
    new Actor('Wolf 2', 50, 0, getMonsterSkills('MON003', 'MON002', 'MON001'), Job.Warrior, 2)
];

const actors = [...heroes, ...villians];

const heroesFighting = actors.filter(el => el.team === 1);
const villansFighting = actors.filter(el => el.team === 2);

const getTargets = (targets: Actor[], take: number) => {
    return targets.filter(el => el.alive).sort((a, b) => b.threat - a.threat).slice(0, take);
};

const logger = new ConsoleLogBuilder();

const engine = new Engine(logger);
function gameLoop() {
    console.log(`------------------Turn ${turn}--------------------`);

    //INITIATIVE
    const turnOrder = actors.filter(el => el.alive);

    //COMBAT
    for (let i = 0; i < turnOrder.length; i++) {
        const actor = turnOrder[i];
        const targets = actor.team === 1 ? getTargets(villansFighting, 1) : getTargets(heroesFighting, 1);

        //PRE-COMBAT
        engine.applyPreCombatDebuffs(actor);

        //COMBAT
        if (canAct(actor)) {
            const skill = actor.getSkill();
            for (let j = 0; j < targets.length; j++) {
                engine.useSkill(actor, skill, targets[j]);
                if (targets[j].hp < 0) {
                    targets[j].alive = false;
                }
            }
        }

        //POST-COMBAT
        engine.applyPostCombatDebuffs(actor);
        actor.progressCooldowns();
    }

    actors.map(el => `${el.name}: ${el.hp}hp`).forEach(el => console.log(el));

    //CHECK END
    if (heroesFighting.every(x => !x.alive) || villansFighting.every(x => !x.alive)) {
        clearInterval(this);
        finish = true;
        console.log('battle over');
    }

    turn++;
}

//setInterval(gameLoop, LOOP_TIME);

const hrstart = process.hrtime();
while (!finish) {
    gameLoop();
}
const hrEnd = process.hrtime(hrstart);
console.info(`Execution time (hr): ${hrEnd[0]} ${hrEnd[1] / 1000000}ms`);



