import * as fastify from 'fastify';
import {FastifyInstance, FastifyReply} from 'fastify';
import {fastifyws} from './plugins/fastify-ws';
import {initDb} from '@db/init-db';
import {RoomService} from './service-plugins/room-service';
import * as redis from 'redis';

require('make-promises-safe');

const client = redis.createClient(6379,'127.0.0.1');

client.on('connect', () => {
    console.log('client connected');
});

const fastifyInit = () => {
    const server: FastifyInstance = fastify({
        logger: true
    });

    server.decorate('setCors', (reply: FastifyReply<any>) => {
        reply.header('Access-Control-Allow-Origin', `*`);
        reply.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    });

    server.addHook('preHandler', (request, reply, done) => {
        (server as any).setCors(reply);
        done();
    });

    server.register(fastifyws);

    server.ready(() => {
        server.ws
            .on('connection', socket => {
                console.log('Client connected.', socket);

                socket.on('message', msg => {console.log(msg); socket.send(msg)}); // Creates an echo server

                socket.on('close', () => console.log('Client disconnected.'));
            });

        server.listen(3001, (err, address) => {
            if (err) throw err;
            server.log.info(`server listening on ${address}`);
        });
    });
};

initDb().then(() => {
    fastifyInit();
}).catch((err) => {
    console.log('Error Connecting to DB:', err);
});
