import * as fp from 'fastify-plugin';
import {FastifyInstance} from 'fastify';
import {Server} from 'ws';

export interface IRoom {
    userIds: string[];
    sockets: WebSocket[];
    id: string;
}
export class RoomService {

    key = 0;
    rooms = [];
    constructor() {

    }

    createRoom = (socket,) => {

    }

    getRoom = () => {

    }
    addUser = (socket, roomId) => {

    }
}

export const fastifyws = fp((fastify: FastifyInstance, opts: any, next) => {

    const ws = new Server({
        server: fastify.server
    })

    fastify.decorate('ws', ws)

    fastify.addHook('onClose', (fastify, done) => fastify.ws.close(done))

    next()
}, {
    fastify: '1.7.0 - 2',
    name: 'fastify-ws'
})