import {Sequelize} from 'sequelize';

export async function initDb() {
    const db = new Sequelize('dev', 'sswan', 'test123', {
        host: '127.0.0.1',
        dialect: 'mssql',
        dialectOptions: {
            instanceName: 'SQLEXPRESS'
        },
    });

    return db.authenticate();
}