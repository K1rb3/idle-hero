"use strict";
exports.__esModule = true;
var fp = require("fastify-plugin");
var ws_1 = require("ws");
exports.fastifyws = fp(function (fastify, opts, next) {
    var ws = new ws_1.Server({
        server: fastify.server
    });
    fastify.decorate('ws', ws);
    fastify.addHook('onClose', function (fastify, done) { return fastify.ws.close(done); });
    next();
}, {
    fastify: '1.7.0 - 2',
    name: 'fastify-ws'
});
