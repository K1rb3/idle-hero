import * as fp from 'fastify-plugin';
import {FastifyInstance} from 'fastify';
import {Server} from 'ws';

export const fastifyws = fp((fastify: FastifyInstance, opts: any, next) => {

    const ws = new Server({
        server: fastify.server
    })

    fastify.decorate('ws', ws)

    fastify.addHook('onClose', (fastify, done) => fastify.ws.close(done))

    next()
}, {
    fastify: '1.7.0 - 2',
    name: 'fastify-ws'
})