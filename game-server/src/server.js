"use strict";
exports.__esModule = true;
var fastify = require("fastify");
var fastify_ws_1 = require("./plugins/fastify-ws");
var init_db_1 = require("@db/init-db");
require('make-promises-safe');
var fastifyInit = function () {
    var server = fastify({
        logger: true
    });
    server.decorate('setCors', function (reply) {
        reply.header('Access-Control-Allow-Origin', "*");
        reply.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    });
    server.addHook('preHandler', function (request, reply, done) {
        server.setCors(reply);
        done();
    });
    server.register(fastify_ws_1.fastifyws);
    server.listen(3001, function (err, address) {
        if (err)
            throw err;
        server.log.info("server listening on " + address);
    });
};
init_db_1.initDb().then(function () {
    fastifyInit();
})["catch"](function (err) {
    console.log('Error Connecting to DB:', err);
});
