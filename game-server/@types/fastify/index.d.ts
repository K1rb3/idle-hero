import * as http from 'http';
import {Server} from 'ws';
import * as fastify from 'fastify';

declare module 'fastify' {
    export interface FastifyInstance<HttpServer = http.Server, HttpRequest = http.IncomingMessage, HttpResponse = http.ServerResponse> {
        ws: Server;
    }
}